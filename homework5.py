#1. Дана довільна строка. Напишіть код, який знайде в
#ній і віведе на екран кількість слів, які містять дві голосні літери підряд.
consonants = ['a', 'u', 'e', 'o', 'y', 'i']
lst2 = []
user_input = input('Enter the text:').split()
a = 1
for i in user_input:
    ii = i.lower()
    for j in range(1, len(i)):
        if ii[j] == ii[j - 1] and ii[j] in consonants:
            lst2.append(i)
            l = len(lst2)
            a = 0
            print(l, i, ' -> ', i[j - 1] + i[j])
if a:
    print(f'Doubling of consonants is not found')

#2. Є два довільних числа які відповідають за мінімальну і максимальну ціну.
#Є Dict з назвами магазинів і цінами:
# {
# "cito": 47.999, "BB_studio" 42.999,
# "momo": 49.999, "main-service": 37.245,
# "buy.now": 38.324, "x-store": 37.166,
# "the_partner": 38.988, "sota": 37.720,
# "rozetka": 38.003 }.
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких попадають в діапазон між мінімальною і максимальною ціною

dict1 = {
    "cito": 47.999, "BB_studio": 42.999,
    "momo": 49.999, "main-service": 37.245,
    "buy.now": 38.324, "x-store": 37.166,
    "the_partner": 38.988, "sota": 37.720, "rozetka": 38.003
}
lower_limit = float(input('Lower limit: '))
upper_limit = float(input('Upper limit: '))
for key, values in list(dict1.items()):
    if lower_limit < values < upper_limit:
        print('Match:', key)

