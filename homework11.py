from abc import ABC, abstractmethod
from typing import Union
from random import randint, choice
from faker import Faker

fake = Faker(locale='uk_UA')


class SchoolPersonal(ABC):
    def __init__(self, name: str, salary: Union[int, float]):
        self.name = name
        self.salary = salary

    @abstractmethod
    def __str__(self):
        pass


class Teacher(SchoolPersonal):
    def __str__(self):
        return f'Мене звати - {self.name}. я вчитель, моя зп -  {self.salary},'


class TechnicalPersonal(SchoolPersonal):
     def __str__(self):
        return f'Мене звати - {self.name}. я прибиральник, моя зп -  {self.salary},'


class School:
    def __init__(self, name: str, principal: Teacher, number_of_teachers: int = 10, number_of_techs: int = 5):
        self.name = name
        self.principal = principal
        self.teachers = [Teacher(fake.name(), randint(10000, 15000)) for position in range(number_of_teachers)]
        self.techs = [TechnicalPersonal(fake.name(), randint(5000, 10000)) for position in range(number_of_techs)]

    @property
    def get_total_salary(self):
        all_personal = []
        all_personal.append(self.principal)
        all_personal += self.teachers
        all_personal += self.techs
        total = sum((obj.salary for obj in all_personal))
        return total

    def set_new_principal(self):
        old_principal = self.principal
        new_candidate = self.teachers.pop()
        self.principal = new_candidate
        self.teachers.append(old_principal)

school1 = School('School1', Teacher('Артур', 12000))
school1.teachers.append(Teacher('Евеліна', 20000))
school1.set_new_principal()
