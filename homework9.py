def print_res(func):
    """"Данний декоратор приймає результат роботи фунцій(bool) та додає принт результату"""
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        if res is True:
            print(args, 'True')
        elif res is False:
            print(args, 'False')
        return res

    return wrapper


@print_res
def even_number(num):
    if num % 2 == 0:
        return True
    elif num % 2 != 0:
        return False


assert even_number(2) is True
assert even_number(1) is False
assert even_number(-2) is True
assert even_number(-1) is False
assert even_number(2 + 2) is True


@print_res
def capitalize_checker(strichka):
    if strichka == strichka.capitalize():
        return True
    elif strichka != strichka.capitalize():
        return False


assert capitalize_checker('qwerty') is False
assert capitalize_checker('Qwerty') is True
assert capitalize_checker('qwertY') is False
assert capitalize_checker('йцукен') is False
assert capitalize_checker('Йцукен') is True


