import datetime
import uuid
from art import *


class banka:
    deposit_portfolio: int = 0

    def __init__(self, name, balance, procent = 2):
        """Initialize self"""
        self.name = name
        self.balance = balance
        self.__procent = procent
        self.__create_date = datetime.date.today()
        self.__token = uuid.uuid4()
        banka.count_of_portfolios()

    def __str__(self):
        return f'{self.name} {self.balance} {self.__procent} {self.__create_date}'

    @staticmethod
    def static_print():
        ''''Реклама'''
        tprint('Banka', font='block', chr_ignore=True)

    @classmethod
    def count_of_portfolios(cls):
        ''''Калькулятор об'єктів в классі'''
        banka.deposit_portfolio += 1

    def set_procent(self, a):
        ''''Сеттер дял зміни відсоткової ставки'''
        self.__procent = a

    def get_procent(self):
        ''''Геттер для відсоткової ставки'''
        return self.__procent

    def get_balance(self):
        ''''Геттр для балансу'''
        return self.balance

    def deposit(self, summa):
        ''''Поповнення балансу'''
        self.balance += summa
        return self.balance

    def withdraw(self, summa):
        ''''Зняття балансу'''
        if self.balance >= summa:
            self.balance -= summa
            return self.balance
        else:
            print('Неможливо здійснити операцію, на вашому рахунку недостаньо коштів')

    def transfer(self, other, summa):
        """Баланс трансфер"""
        if self.balance >= summa:
            self.balance -= summa
            other.balance += summa
        else:
            print('Неможливо здійснити операцію, на вашому рахунку недостаньо коштів')

    @classmethod
    def close_acc(cls):
        ''''Закриття акануту'''
        banka.deposit_portfolio -= 1

    def __del__(self):
        ''''Видалення об'єкту супутно обнуляючи рахунок'''
        balance_before_closing = self.balance
        self.balance = 0
        banka.close_acc()
        print(self, f': Акаунт закрито, вам будуть повернуть ваші кошти - {balance_before_closing}')

    @property
    def get_todays_profit(self):
        ''''Розрахунок та начислення відсоткової ставки за один день'''
        self.balance = self.balance + self.balance * self.__procent / 365
        return self.balance


Artur = banka('Artur', 1000) # Створення

Evelina = banka('Evelina', 1000)
Evelina.set_procent(5) # Призначення відсотку
Evelina.deposit(1000) # Поповнення
Evelina.withdraw(100) # Зняття
#print(test.get_balance()) # геттер балансу

Artur.transfer(Evelina, 100) # Трансфер грошей


Evelina.get_todays_profit
Artur.get_todays_profit
banka.static_print()