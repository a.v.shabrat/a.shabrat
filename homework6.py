from pprint import pprint
from tabulate import tabulate
import requests

#Task №1
#Вивести список всіх астронавтів, що перебувають в даний момент на орбіті
url = 'http://api.open-notify.org/astros.json'
res = requests.get(url)
resl = res.json()['people']
for key in resl:
    if 'name' in key:
        print(key['name'])



#Task №2
#Роздрукувати тепрературу та швидкість вітру. з вказівкою міста, яке було вибране
city = input('Введіть назву міста:\n')
url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'
res = requests.get(url)
print('Температура:', res.json()['main']['temp'])
print('Швидкість вітру:', res.json()['wind']['speed'])