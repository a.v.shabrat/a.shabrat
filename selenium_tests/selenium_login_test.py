from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time


def test_01():
    options = Options()
    options.add_argument("start-maximized")
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

    driver.get("https://uakino.club/")
    login = 'hw14'
    password = 'Qwerty123.'
    login_btn = '//i[@class="fa fa-user"]'
    driver.find_element(By.XPATH, login_btn).click()
    time.sleep(3)
    login_input = 'login_name'
    driver.find_element(By.ID, login_input).send_keys(login)
    password_input = 'login_password'
    driver.find_element(By.ID, password_input).send_keys(password)
    enter_to_site_btn = '//button[@onclick="submit();"]'
    driver.find_element(By.XPATH, enter_to_site_btn).click()
    time.sleep(3)
    verivy_login = '//i[@class="fa fa-cog"]'
    verivy_login_element = driver.find_element(By.XPATH, verivy_login)
    is_my_profile_btn = verivy_login_element.is_displayed()
    assert is_my_profile_btn is True, f'User is not login'
    driver.quit()
