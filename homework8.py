#1. Напишіть декоратор, який перетворює результат роботи функції на стрінг
#2. Напишіть докстрінг для цього декоратора
def my_decorator(func):
    """"Данний декоратор приймає результат роботи фунції 'foo'(float) та перетворює його на str"""
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        str_output = str(res)
        print(type(str_output), str_output)
        return res

    return wrapper

@my_decorator
def foo(a):
    return 100 / a - 5

foo(10)