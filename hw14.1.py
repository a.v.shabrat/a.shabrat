# https://www.google.com/

# XPath:
# 1.//body[@jsmodel="hspDDf"]
# 2.//a[@class="gb_d"]/../..
# 3.//a[@data-pid="2"]
# 4.//a[@class="gb_A"]
# 5.//a[@class="gb_2 gb_3 gb_9d gb_9c"]
# 6.//input[@class="gLFyf gsfi"]
# 7.//span[@class="ly0Ckb"]
# 8.//div[@jscontroller="unV4T"]
# 9.//input[@data-ved="0ahUKEwiax_LMn6T7AhVSC-wKHVGxB4kQ4dUDCA0"]
# 10.//input[@data-ved="0ahUKEwiax_LMn6T7AhVSC-wKHVGxB4kQ19QECA4"]
# 11.//div[@id="tophf"]//input[@name="source"]
# 12.//div[@id="tophf"]//input[@value="ZUNtY9qAA9KWsAfR4p7ICA"]
# 13.//div[@id="tophf"]//input[@name="iflsig"]
# 14.//div[@class="o3j99 qarstb"]//div[@class="vcVZ7d"]
# 15.//div[@id="SIvCob"]
# 16.//div[@class="KxwPGc iTjxkf"]
# 17.//div[@class="Fgvgjc"]
# 18.//*[@class="Fgvgjc"]
# 19.//*[@class="csi"]
# 20.//*[@class="o3j99 ikrT4e om7nvf"]
# 21.//*[@jsmodel="hspDDf"]
# 22.//input[@name="q"]//ancestor::div[@class="L3eUgb"]
# 23.//input[@name="q"]//ancestor::div[@class="o3j99 ikrT4e om7nvf"]
# 24.//input[@name="q"]//ancestor::div[@class="RNNXgb"]
# 25.//div[@class="L3eUgb"]/child::div[@class="o3j99 qarstb"]
# 26.//div[@class="L3eUgb"]/child::div[@class="o3j99 c93Gbe"]
# 27.//dialog[@id="spch-dlg"]
# 28.//div[@class="spch"]
# 29.//div[@class="L3eUgb"]/parent::*
# 30.//a[contains(text(), 'Реклама')]


# CSS
# 1.div.gb_Se
# 2.div.LX3sZb
# 3.div.gb_Se
# 4.div.gb_Tc
# 5.div.L3eUgb
# 6.div.gb_Wd.gb_Za.gb_Ld
# 7.#spch-dlg
# 8.#spch
# 9.#spchx
# 10.#_ij
# 11.div div div.LX3sZb
# 12.div div div.gb_pa.gb_Zd.gb_2a.gb_Qd
# 13.[class="gb_Wd gb_Za gb_Ld"]
# 14.div[class="gb_pa gb_Zd gb_2a gb_Qd"]
# 15.div[class~="gb_pa"]