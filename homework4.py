#1Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
#Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг,
#які присутні в lst1. Зауважте, що lst1 не є статичним і може формуватися динамічно.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for string in lst1:
    if type(string) is str:
        lst2.append(string)
print(lst2)

#2 Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.

my_list1 = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
my_list1 = [my_list1.pop(i) for i in reversed(range(len(my_list1))) if 21 < my_list1[i] < 75]
print(my_list1)

#3 Є стрінг з певним текстом (можна скористатися input або константою).
#Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

user_input = input('Enter the text: ')
a = 0
while user_input:
    user_input = user_input.lower()
    wordO = user_input.find('o')
    if wordO == -1:
        break
    elif wordO == (len(user_input) - 1):
        a += 1
        break
    elif (user_input[wordO+1] == ' ') or (user_input[wordO+1] == chr(44)) or (user_input[wordO+1] == chr(46)):
        a += 1
        user_input = user_input[wordO+1:]
    else:
        user_input = user_input[wordO+1:]
if a == 0:
    print('No words ending with "o" were found')
else:
    print('The number of words ending in "o"' % a)

