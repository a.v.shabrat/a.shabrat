#Задача: Створіть дві змінні first=10, second=30.
#Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

task1 = 'Задача №1'
print(task1)

first = 10
second = 30

addition = first + second
print('Додавання:', addition)

subtraction = first - second
print('Віднімання:', subtraction)

multiplication = first * second
print('Множення:', multiplication)

division = first / second
print('Ділення:', division)

exponentiation = first ** second
print("Зведення в ступінь:", exponentiation)

integer_division = first // second
print('Цілочисельне ділення:', integer_division)

modulo_division = first % second
print('Ділення по модулю:', modulo_division)

#Задача: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
#Виведіть на екран результат кожного порівняння.

task2 = 'Задача №2'
print(task2)

boolean = first < second
print('10 менше за 30:', boolean)

boolean = first > second
print('10 більше за 30:', boolean)

boolean = first >= second
print('10 більше або рівне 30:', boolean)

boolean = first <= second
print('10 менше або рівне 30:', boolean)

boolean = first == second
print('10 в точності рівне 30:', boolean)

boolean = first != second
print('10 не рівне 30:', boolean)













