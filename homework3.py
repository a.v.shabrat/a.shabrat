#1
#Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
#Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
#Слово та номер отримайте за допомогою input() або скористайтеся константою.
#Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".
try:
    word = input('Введіть слово: \n')
    index_word = int(input('Введіть порядковий номер за яким ви хочете знайти символ: \n'))
    print(f'The {index_word} symbol in {word} is {word[index_word]}')
except IndexError as error:
    print('Помилка: Введено неіснуючий порядковий номер для данного слова')
except ValueError as error:
    print('Помилка: Невірний тип данних. Наступного разу спробуйте з Int`ом')

#Написати цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються як великі так і маленькі).
#Цикл не повинен завершитися, якщо користувач ввів слово без букви о.

while True:
    user_input = input('Enter the word with letter - "о"')
    x = 'o'
    y = 'O'
    l = len(user_input)
    if x in user_input or y in user_input:
        print(f'Thank!')
        break
    else:
        print(f'Try again')

